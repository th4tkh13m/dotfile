vim.opt.shadafile = "NONE"
--Load options, mappings
local doom_modules = {
    "options",
    "mappings",
}
for i = 1, #doom_modules, 1 do
   pcall(require, doom_modules[i])
end

if vim.g.vscode == nil and vim.g.started_by_firenvim == nil then
    --load impatient first
    pcall(require, "impatient")

    -- load plugins
    pcall(require, "packer_compiled")
end

vim.opt.shadafile = ""
