local M = {}
local api = vim.api
local cmd = vim.cmd
-- This is tidy.nvim plugin
function M.tidy_up()

    -- get tuple of cursor position before making changes
    local pos = api.nvim_win_get_cursor( 0 )

    -- delete all whitespace, see source 1
    cmd[[:keepjumps keeppatterns %s/\s\+$//e]]

    -- delete all lines at end of buffer, see source 2
    cmd[[:keepjumps keeppatterns silent! 0;/^\%(\n*.\)\@!/,$d]]

    -- get row count after line deletion
    local end_row = api.nvim_buf_line_count( 0 )


    --[[
        if the row value in the original cursor
        position tuple is greater than the
        line count after empty line deletion
        (meaning that the cursor was inside of
        the group of empty lines at the end of
        the file when they were deleted), set
        the cursor row to the last line
    ]]
    if pos[1] > end_row then
        pos[1] = end_row
    end

    api.nvim_win_set_cursor( 0, pos )
end
function M.attach()
    local bufnr = api.nvim_get_current_buf()
    vim.lsp.buf_attach_client(bufnr, 1)
end
return M
