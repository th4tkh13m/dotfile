local opt = vim.opt
local g = vim.g
local cmd = vim.cmd

opt.title = true
opt.clipboard = "unnamedplus"
opt.cmdheight = 1
-- opt.cul = true -- cursor line

-- Indentline
opt.expandtab = true
opt.shiftwidth = 4
opt.smartindent = true

opt.fillchars = { eob = " " }
opt.list = true
-- opt.listchars:append("eol:↴")
opt.shell = "/bin/bash"

opt.hidden = true
opt.ignorecase = true
opt.smartcase = true
opt.mouse = "a"

-- Numbers
opt.number = true
opt.numberwidth = 2
opt.relativenumber = false
opt.ruler = false

-- disable nvim intro
opt.shortmess:append "sI"
-- opt.showtabline = 2

opt.signcolumn = "yes"
opt.splitbelow = true
opt.splitright = true
opt.tabstop = 4
opt.termguicolors = true
opt.timeoutlen = 400
opt.undofile = true
opt.lazyredraw = true
opt.showmode = false
opt.autowriteall = true
opt.swapfile = false

-- Folding
opt.foldmethod = 'expr'
opt.foldexpr = 'nvim_treesitter#foldexpr()'
opt.foldlevel = 99

-- interval for writing swap file to disk, also used by gitsigns
opt.updatetime = 250

-- go to previous/next line with h,l,left arrow and right arrow
-- when cursor reaches end/beginning of line
opt.whichwrap:append "<>[]hl"

g.mapleader = ","

g.did_load_filetypes = 1
-- disable some builtin vim plugins
local disabled_built_ins = {
   "gzip",
   "netrwPlugin",
   "matchit",
   "tarPlugin",
   "zipPlugin",
}

for i = 1, #disabled_built_ins, 1 do
   g["loaded_" .. disabled_built_ins[i]] = 1
end

-- Plugins related
g.copilot_no_tab_map = true
g.copilot_assume_mapped = true

g.symbols_outline = {
    highlight_hovered_item = true,
    show_guides = true,
    auto_preview = false,
    position = 'right',
    relative_width = true,
    width = 30,
    show_numbers = false,
    show_relative_numbers = false,
    show_symbol_details = true,
    preview_bg_highlight = 'Pmenu',
    keymaps = { -- These keymaps can be a string or a table for multiple keys
        close = {"<Esc>", "q"},
        goto_location = "<Cr>",
        focus_location = "o",
        hover_symbol = "<C-space>",
        toggle_preview = "K",
        rename_symbol = "r",
        code_actions = "a",
    },
    lsp_blacklist = {},
    symbol_blacklist = {},
    symbols = {
        File = {icon = '  ', hl = "TSURI"},
        Module = {icon = '  ', hl = "TSNamespace"},
        Namespace = {icon = "", hl = "TSNamespace"},
        Package = {icon = "", hl = "TSNamespace"},
        Class = {icon = '  ', hl = "TSType"},
        Method = {icon = '  ', hl = "TSMethod"},
        Property = {icon = '  ', hl = "TSMethod"},
        Field = {icon = '  ', hl = "TSField"},
        Constructor = {icon = '  ', hl = "TSConstructor"},
        Enum = {icon = '  ', hl = "TSType"},
        Interface = {icon = '  ', hl = "TSType"},
        Function = {icon = '  ', hl = "TSFunction"},
        Variable = {icon = '  ', hl = "TSConstant"},
        Constant = {icon = '  ', hl = "TSConstant"},
        String = {icon = '  ', hl = "TSString"},
        Number = {icon = "#", hl = "TSNumber"},
        Boolean = {icon = "B", hl = "TSBoolean"},
        Array = {icon = "", hl = "TSConstant"},
        Object = {icon = "⦿", hl = "TSType"},
        Key = {icon = "", hl = "TSType"},
        Null = {icon = "NULL", hl = "TSType"},
        EnumMember = {icon = "", hl = "TSField"},
        Struct = {icon = '  ', hl = "TSType"},
        Event = {icon = '  ', hl = "TSType"},
        Operator = {icon = '  ', hl = "TSOperator"},
        TypeParameter = {icon = '  ', hl = "TSParameter"}
    }
}
g.vimwiki_list = {{path = '~/Documents/Mywiki', syntax = 'markdown', ext = '.md'}}
g.onenord_disable_background = true
-- Python Env
g.python3_host_prog='/home/th4tkh13m/.programming/miniconda3/envs/myenv/bin/python3'


-- Don't show any numbers inside terminals
cmd [[ au TermOpen term://* setlocal nonumber norelativenumber | setfiletype terminal ]]
