local present, lualine = pcall(require, "lualine")

if not present then
   return
end
lualine.setup {
    options = {
        theme = 'nord'
    },
    tabline = {
        lualine_b = {},
        lualine_c = {},
        lualine_x = {},
        lualine_y = {},
        lualine_z = {'tabs'},
    },
    extenstions = {'nvim-tree', 'symbols-outline'}
}
