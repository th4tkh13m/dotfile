local present, null_ls = pcall(require, "null-ls")
if not present then
   return
end

local b = null_ls.builtins

local sources = {
    b.formatting.prettierd.with { filetypes = { "html", "markdown", "css" } },
    b.diagnostics.shellcheck,
    b.formatting.yapf,
    -- b.diagnostics.pylint,
    b.code_actions.refactoring,
    b.formatting.sqlformat.with({
        extra_args = {"-a"}
    }),
}
null_ls.setup {
    sources = sources,
    diagnostics_format = "[#{c}] #{m} (#{s})",

    -- format on save
    on_attach = require("configs.lspconfig").on_attach,
}
