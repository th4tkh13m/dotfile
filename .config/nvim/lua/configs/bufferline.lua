local present, bufferline = pcall(require, "bufferline")
if not present then
   return
end

bufferline.setup {
    options = {
        buffer_close_icon = "",
        modified_icon = "",
        close_icon = "",
        left_trunc_marker = "",
        right_trunc_marker = "",
        indicator_icon = '▎',
        max_name_length = 18,
        max_prefix_length = 15, -- prefix used when a buffer is de-duplicated
        tab_size = 18,
        diagnostics =  "nvim_lsp" ,
        diagnostics_update_in_insert = false,
        offsets = {{filetype = "NvimTree", text = "File Explorer"  , text_align = "left" },
        show_buffer_icons = true , -- disable filetype icons for buffers
        show_buffer_close_icons = true ,
        show_close_icon = true ,
        show_tab_indicators = true ,
        persist_buffer_sort = true, -- whether or not custom sorted buffers should persist
        separator_style = "slant" ,
        enforce_regular_tabs = false ,
        always_show_bufferline = true ,
        }
    }
}
