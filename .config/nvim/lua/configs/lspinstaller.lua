local present, lsp_installer = pcall(require, "nvim-lsp-installer")

if not present then
   return
end

lsp_installer.on_server_ready(function(server)
    local default_opts = {
        on_attach = require("configs.lspconfig").on_attach,
        capabilities = require("configs.lspconfig").capabilities,
    }
      -- Now we'll create a server_opts table where we'll specify our custom LSP server configuration
    local server_opts = {
     -- Provide settings that should only apply to the "eslintls" server
    ["sumneko_lua"] = function()
        default_opts.settings = {
            Lua = {
                diagnostics = {
                    globals = { 'vim' }
                }
            }
        }
    end,
    }

  -- Use the server's custom settings, if they exist, otherwise default to the default options
  local server_options = server_opts[server.name] and server_opts[server.name]() or default_opts
    server:setup(server_options)
    vim.cmd [[ do User LspAttachBuffers ]]
end)

-- Include the servers you want to have installed by default below
local servers = {
    "pyright",
    "bashls",
    "sumneko_lua",
    -- "grammarly",
    "texlab"
}

for _, name in pairs(servers) do
    local server_is_found, server = lsp_installer.get_server(name)
    if server_is_found then
        if not server:is_installed() then
            print("Installing " .. name)
            server:install()
        end
    end
end
