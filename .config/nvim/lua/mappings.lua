-- helper function for clean mappings
local function map(mode, lhs, rhs, opts)
   local options = { noremap = true, silent = true }
   if opts then
      options = vim.tbl_extend("force", options, opts)
   end
   vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

local cmd = vim.cmd

local M = {}

local function non_config_mappings()
    -- Don't copy the replaced text after pasting in visual mode
    map("v", "p", '"_dP')

    -- use ESC to turn off search highlighting
    map("n", "<Esc>", ":noh <CR>")
end

local function optional_mappings()
    -- don't yank text on delete ( dd )
    map("n", "d", '"_d')
    map("v", "d", '"_d')
    -- navigation within insert mode
    map("i", "<C-h>", "<Left>")
    map("i", "<C-e>", "<End>")
    map("i", "<C-l>", "<Right>")
    map("i", "<C-k>", "<Up>")
    map("i", "<C-j>", "<Down>")
    map("i", "<C-a>", "<ESC>^i")

    -- Vmap for maintain Visual Mode after shifting > and <
    map("v", "<", "<gv")
    map("v", ">", ">gv")

    map("v", "J", ":m '>+1<CR>==gv")
    map("v", "K", ":m '<-2<CR>==gv")
end

local function required_mappings()
    map("n", "<C-a>", ":%y+ <CR>") -- copy whole file content
    map("n", "<S-t>", ":enew <CR>") -- new buffer
    map("n", "<C-t>b", ":tabnew <CR>") -- new tabs
    map("n", "<leader>n", ":set nu! <CR>") -- toggle numbers
    map("n", "<C-s>", ":w <CR>") -- ctrl + s to save file
    map("n", "<leader>x", ":tabclose <CR>") -- close tab

    --Buffers
    map("n", "<TAB>", ":bnext <CR>")
    map("n", "<S-TAB>", ":bNext <CR>")
    map("n", "<leader>bc", ":bdelete <CR>")

    -- Tex compiling and viewing
    map("n", "<leader>cc", ":w !pdflatex %:r.tex && bibtex %:r.aux && pdflatex %:r.tex && pdflatex %:r.tex && rm %:r.aux %:r.log %:r.blg %:r.bbl <CR>")
    map("n", "<leader>cv", ":!zathura %:r.pdf > /dev/null 2>&1 & <CR>")

    -- terminal mappings --
    -- get out of terminal mode
    map("t",  "jk" , "<C-\\><C-n>")
    -- pick a hidden term
    map("n", "<leader>W", ":Telescope terms <CR>")
    -- Open terminals
    map("n", "<leader>th", ":execute 15 .. 'new +terminal' | let b:term_type = 'hori' | startinsert <CR>")
    map("n", "<leader>tv", ":execute 'vnew +terminal' | let b:term_type = 'vert' | startinsert <CR>")
    map("n", "<leader>tb", ":execute 'terminal' | let b:term_type = 'wind' | startinsert <CR>")
    -- terminal mappings end --

    -- Add Packer commands because we are not loading it at startup
    cmd "silent! command PackerClean lua require 'pluginList' require('packer').clean()"
    cmd "silent! command PackerCompile lua require 'pluginList' require('packer').compile()"
    cmd "silent! command PackerInstall lua require 'pluginList' require('packer').install()"
    cmd "silent! command PackerStatus lua require 'pluginList' require('packer').status()"
    cmd "silent! command PackerSync lua require 'pluginList' require('packer').sync()"
    cmd "silent! command PackerUpdate lua require 'pluginList' require('packer').update()"

    cmd('silent! command FTermOpen lua require("FTerm").open()')
    cmd('silent! command FTermClose lua require("FTerm").close()')
    cmd('silent! command FTermExit lua require("FTerm").exit()')
    cmd('silent! command FTermToggle lua require("FTerm").toggle()')
end

non_config_mappings()
optional_mappings()
required_mappings()

-- below are all plugin related mappings
M.bufferline = function()
   map("n", "<TAB>", ":BufferLineCycleNext <CR>")
   map("n", "<S-TAB>", ":BufferLineCyclePrev <CR>")
   map("n", "<leader>bc", ":BufferLinePickClose <CR>")
end

M.angry_reviewer = function ()
    map("n", "<leader>r", ":AngryReviewer <CR>")
end

M.hop = function ()
    map("n", "<space>w", ":HopWord <CR>")
    map("n", "<space>l", ":HopLine <CR>")
    map("n", "<space>/", ":HopPattern <CR>")
end

M.fugitive = function ()
    map("n", "<space>gf", ":Git fetch <CR>")
    map("n", "<space>ga", ":Git add % <CR>")
    map("n", "<space>gA", ":Git add -A <CR>")
    map("n", "<space>gc", ":Git commit <CR>")
    map("n", "<space>gp", ":Git push <CR>")
    map("n", "<space>gP", ":Git pull <CR>")
end

M.symbols_outline = function ()
    map("n", "<leader>a", ":SymbolsOutline <CR>")
end

M.comment = function()
   local m = "<leader>/"
    map("n", m, ":CommentToggle <CR>")
    map("v", m, ":CommentToggle <CR>")
end

M.trouble = function ()
    map("n", "<space>t", ":TroubleToggle workspace_diagnostics<CR>")
end

M.todo = function ()
    map("n", "<space>r", ":TodoTrouble <CR>")
end

M.true_zen = function ()
    map("n", "<F1>", ":TZMinimalist <CR>")
    map("n", "<F2>", ":TZFocus <CR>")
    map("n", "<F3>", ":TZAtaraxis <CR>")
end

M.nvimtree = function()
    map("n", "<leader>q", ":NvimTreeToggle <CR>")
    map("n", "<leader>e", ":NvimTreeFocus <CR>")
end

M.telescope = function()
    map("n", "<leader>fb", ":Telescope buffers <CR>")
    map("n", "<leader>ff", ":Telescope find_files <CR>")
    map("n", "<leader>fa", ":Telescope find_files hidden=true <CR>")
    map("n", "<leader>gc", ":Telescope git_commits <CR>")
    map("n", "<leader>gt", ":Telescope git_status <CR>")
    map("n", "<leader>fh", ":Telescope help_tags <CR>")
    map("n", "<leader>fw", ":Telescope live_grep <CR>")
    map("n", "<leader>fo", ":Telescope oldfiles <CR>")
end

M.telescope_media = function()
   map("n", "<leader>fm", ":lua require('telescope').extensions.media_files.media_files() <CR>")
end

M.telescope_project =function ()
    map("n", "<leader>fp", ":lua require'telescope'.extensions.project.project{}<CR>")
end

M.copilot = function ()
    map("n", "<leader>c", ":Copilot <CR>")
end

M.dap = function ()
    map("n", "<leader>db", ":lua require'dap'.toggle_breakpoint() <CR>")
    map("n", "<leader>dc", ":lua require'dap'.continue() <CR>")
    map("n", "<leader>do", ":lua require'dap'.step_over() <CR>")
    map("n", "<leader>di", ":lua require'dap'.step_into() <CR>")
    map("n", "<leader>dr", ":lua require'dap'.repl.open() <CR>")
end

M.vimwiki = function ()
    map("n", "<leader>ww", ":VimwikiIndex <CR>")
end

M.dapui = function ()
    map("n", "<leader>dt", ":lua require('dapui').toggle() <CR>")
end

M.auto_session = function ()
    map("n", "<leader>ss", ":SaveSession <CR>" )
    map("n", "<leader>sr", ":RestoreSession <CR>" )
end

-- venn.nvim: enable or disable keymappings
M.venn = function()
    local venn_enabled = vim.inspect(vim.b.venn_enabled)
    if venn_enabled == "nil" then
        vim.b.venn_enabled = true
        vim.cmd[[setlocal ve=all]]
        -- draw a line on HJKL keystokes
        vim.api.nvim_buf_set_keymap(0, "n", "<S-j>", "<C-v>j:VBox<CR>", {noremap = true})
        vim.api.nvim_buf_set_keymap(0, "n", "<S-k>", "<C-v>k:VBox<CR>", {noremap = true})
        vim.api.nvim_buf_set_keymap(0, "n", "<S-l>", "<C-v>l:VBox<CR>", {noremap = true})
        vim.api.nvim_buf_set_keymap(0, "n", "<S-h>", "<C-v>h:VBox<CR>", {noremap = true})
        -- draw a box by pressing "f" with visual selection
        vim.api.nvim_buf_set_keymap(0, "v", "<C-f>", ":VBox<CR>", {noremap = true})
    else
        vim.cmd[[setlocal ve=]]
        vim.cmd[[mapclear <buffer>]]
        vim.b.venn_enabled = nil
    end
end
return M
