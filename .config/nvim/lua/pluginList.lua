local present, packer = pcall(require, "packerInit")

if not present then
    print("packerInit not found")
    return false
end

local use = packer.use

return packer.startup(function()
    -- Fast startup
    use 'lewis6991/impatient.nvim'

    -- Using lua filetype
    use "nathom/filetype.nvim"

    -- Useful Lua functions
    use "nvim-lua/plenary.nvim"

    -- Packer
    use {"wbthomason/packer.nvim",
        event = "VimEnter",
    }

    -- UI
    -- OneNord theme
    use {'https://gitlab.com/th4tkh13m/onenord-nvim.git',
        as = "onenord.nvim",
        config = function()
            require("onenord").set()
        end,
        after = "packer.nvim"
    }

    use {'kyazdani42/nvim-web-devicons',
        after = "onenord.nvim",
        config = function ()
            require("nvim-web-devicons").setup()
        end
    }

 -- Lualine statusline
    use {'hoob3rt/lualine.nvim',
        config = function()
            require 'lualine'.setup {
                options = {theme = 'nord'},
                extenstions = {'nvim-tree', 'symbols-outline'}
            }
        end,
        after = 'nvim-web-devicons'
    }

    use {"akinsho/bufferline.nvim",
        after = "nvim-web-devicons",
        config = function ()
            require("configs.bufferline")
        end,
        setup = function()
            require("mappings").bufferline()
        end,
    }


    -- Syntax highlighting
    use {"nvim-treesitter/nvim-treesitter",
        event = "BufRead",
        config = function()
            require("configs.treesitter")
        end,
    }

    -- git stuff
    use {"lewis6991/gitsigns.nvim",
        opt = true,
        config = function()
            require("configs.gitsigns")
        end,
        setup = function()
            vim.defer_fn(function()
                require("packer").loader("gitsigns.nvim")
            end, 0)
        end,
    }

    use {"tpope/vim-fugitive",
        cmd = "Git",
        setup = function ()
            require("mappings").fugitive()
        end
    }

    -- lsp stuff

    use {"neovim/nvim-lspconfig",
        opt = true,
        setup = function()
            vim.defer_fn(function()
                require("packer").loader("nvim-lspconfig")
            end, 0)
            -- reload the current file so lsp actually starts for it
            vim.defer_fn(function()
            vim.cmd 'if &ft == "packer" | echo "" | else | silent! e %'
            end, 0)
        end,
        config = function()
            vim.api.nvim_exec( [[
                    augroup jdtls_lsp
                    autocmd!
                    autocmd FileType java lua require'configs.jdtls'
                    autocmd BufRead *.java lua require'utils'.attach()
                    augroup end
            ]], true)
        end,
    }

    -- Jdtls setup
    use {'mfussenegger/nvim-jdtls',
        ft = {'java', 'jar'},
    }


     use {"jose-elias-alvarez/null-ls.nvim",
        after = "nvim-lspconfig",
        config = function()
            require("configs.null-ls")
        end,
    }

    use {"ThePrimeagen/refactoring.nvim",
        after = "null-ls.nvim",
    }

    use {'williamboman/nvim-lsp-installer',
        after = "nvim-lspconfig",
        config = function ()
            require("configs.lspinstaller")
        end
    }

    use {"ray-x/lsp_signature.nvim",
        after = "nvim-lspconfig",
        config = function()
            require("configs.others").signature()
        end,
    }

    use {'stevearc/dressing.nvim',
        after = "null-ls.nvim",
    }

    use {'folke/trouble.nvim',
        cmd = "TroubleToggle",
        config = function ()
            require("configs.trouble")
        end,
        setup = function ()
            require("mappings").trouble()
        end
    }

    -- load luasnips + cmp related in insert mode only
    use {"github/copilot.vim",
        cmd = "Copilot",
        setup = function()
            require("mappings").copilot()
        end,
    }

    use {"rafamadriz/friendly-snippets",
        event = "InsertEnter",
    }

    use {"hrsh7th/nvim-cmp",
        after = "friendly-snippets",
        config = function()
            require("configs.cmp")
        end,
    }

    use {"L3MON4D3/LuaSnip",
        wants = "friendly-snippets",
        after = "nvim-cmp",
        config = function()
            require("configs.others").luasnip()
        end,
    }

    use {"saadparwaiz1/cmp_luasnip",
        after = "LuaSnip",
    }

    use {"hrsh7th/cmp-nvim-lua",
        after = "cmp_luasnip",
    }

    use {"hrsh7th/cmp-nvim-lsp",
        after = "cmp-nvim-lua",
    }

    use {"hrsh7th/cmp-buffer",
        after = "cmp-nvim-lsp",
    }

    use {"hrsh7th/cmp-path",
        after = "cmp-buffer",
    }


    use {'mfussenegger/nvim-dap',
        event = "BufRead",
        setup = function ()
            require("mappings").dap()
        end
    }

    use { "rcarriga/nvim-dap-ui",
        after = "nvim-dap",
        config = function ()
            require("dapui").setup()
        end,
        setup = function ()
            require("mappings").dapui()
        end
    }

    -- file managing , picker etc
    use {"kyazdani42/nvim-tree.lua",
        cmd = { "NvimTreeToggle", "NvimTreeFocus" },
        config = function()
            require("configs.nvimtree")
        end,
        setup = function()
            require("mappings").nvimtree()
        end,
    }


    use {"nvim-telescope/telescope.nvim",
        module = "telescope",
        cmd = "Telescope",
        requires = {
            {"nvim-telescope/telescope-fzf-native.nvim",
                after = "nvim-lspconfig",
                run = "make",
            },
            {"nvim-telescope/telescope-media-files.nvim",
                after = "telescope.nvim",
                setup = function()
                    require("mappings").telescope_media()
                end,
            },
            {"nvim-telescope/telescope-project.nvim",
                after = "telescope.nvim",
                setup = function()
                    require("mappings").telescope_project()
                end,
            },
        },
        config = function()
            require("configs.telescope")
        end,
        setup = function()
             require("mappings").telescope()
        end,
    }

    -- Others useful plugins

    use {"terrortylor/nvim-comment",
        cmd = "CommentToggle",
        config = function()
            require("configs.others").comment()
        end,
        setup = function()
             require("mappings").comment()
        end,
    }


    use {"windwp/nvim-autopairs",
        after = "nvim-cmp",
        config = function()
            require("configs.others").autopairs()
        end,
    }

    use {"andymass/vim-matchup",
        opt = true,
        setup = function()
            vim.defer_fn(function()
                require("packer").loader("vim-matchup")
            end, 0)
        end,
    }

    use {"folke/todo-comments.nvim",
        requires = "nvim-lua/plenary.nvim",
        event = "BufRead",
        config = function()
            require("todo-comments").setup {}
        end,
        setup = function ()
            require("mappings").todo()
        end
    }

    use {"max397574/better-escape.nvim",
        event = "InsertEnter",
        config = function()
            require("configs.others").better_escape()
        end,
    }

    -- Hop, use for easy motion
    use {"phaazon/hop.nvim",
        cmd = {
            "HopWord",
            "HopLine",
            "HopChar1",
            "HopChar2",
            "HopPattern",
        },
        config = function()
             require("hop").setup()
        end,
        setup = function ()
            require("mappings").hop()
        end
    }

    -- Indenting
    use {"lukas-reineke/indent-blankline.nvim",
        event = "BufRead",
        config = function()
            require("configs.others").blankline()
        end,
    }

    -- Smooth scrolling
    use {"karb94/neoscroll.nvim",
        opt = true,
        config = function()
            require("neoscroll").setup()
        end,
        -- lazy loading
        setup = function()
            vim.defer_fn(function()
                require("packer").loader("neoscroll.nvim")
            end, 0)
        end,
    }

    -- Stable buffer when opening windows
    use {"luukvbaal/stabilize.nvim",
        config = function()
            require("stabilize").setup()
        end,
        after = "nvim-lspconfig",
    }

    -- Zen mode
    use {"Pocco81/TrueZen.nvim",
        cmd = {
            "TZAtaraxis",
            "TZMinimalist",
            "TZFocus",
        },
        config = function()
            require('configs.true_zen')
        end,
        setup = function()
            require("mappings").true_zen()
        end,
    }

    -- Show variables, functions, etc
    use {'simrat39/symbols-outline.nvim',
        cmd = "SymbolsOutline",
        setup = function ()
            require('mappings').symbols_outline()
        end,
    }

    -- Scientific writing check
    use {'anufrievroman/vim-angry-reviewer',
        cmd = "AngryReviewer",
        config = function ()
            vim.g.AngryReviewerEnglish = 'american'
        end,
        setup = function ()
            require("mappings").angry_reviewer()
        end,
    }


    use({"aserowy/tmux.nvim",
        event = "BufRead",
        config = function()
            require("tmux").setup({
                copy_sync = {
                    enable = true,
                },
                navigation = {
                    enable_default_keybindings = true,
                },
                resize = {
                    enable_default_keybindings = true,
                }
            })
        end,
    })

    use {'norcalli/nvim-colorizer.lua',
        event = "BufEnter",
        config = function()
            require("colorizer").setup()
        end,
    }

    use {'vimwiki/vimwiki',
        cmd = "VimwikiIndex",
        setup = function ()
            require("mappings").vimwiki()
        end,
    }

    use {'kevinhwang91/nvim-hlslens',
        event = "BufRead",
    }

    use {"jbyuki/venn.nvim",
        cmd = "VBox",
        setup = function ()
            require("mappings").venn()
        end,
    }

    use {"tpope/vim-surround",
        event = "BufRead"
    }

end)
