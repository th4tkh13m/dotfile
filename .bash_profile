# .bash_profile

# Get the aliases and functions
[ -f $HOME/.bashrc ] && . $HOME/.bashrc
if [ ! -d "/tmp/$(id -u)" ]
then
    mkdir /tmp/$(id -u)
fi
GPG_TTY=$(tty)
export GPG_TTY
export XDG_RUNTIME_DIR=/tmp/$(id -u)
