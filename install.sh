#!/bin/dash
# INSTALL NEEDED PACKAGES
# doas pacman -Rsu $(comm -23 <(pacman -Qq | sort) <(sort ./software/pkglist.txt))
# doas pacman -Syu

cat << EOF > ~/.bashrc
alias v="nvim"
alias poweroff="doas poweroff"
alias suspend="doas suspend"
alias reboot="doas reboot"
alias xr="doas xbps-remove"
alias xq="doas xbps-query"
export RANGER_LOAD_DEFAULT_RC=false
export EDITOR=nvim
EOF

mkdir ~/dotfile_downloads

## SETUP FONTS
aria2c https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/JetBrainsMono.zip --dir=~/dotfile_downloads
doas mkdir /usr/share/fonts
doas mkdir /usr/share/fonts/JetBrainsMono
doas unzip ~/dotfile_downloads/JetBrainsMono.zip -d /usr/share/fonts/JetBrainsMono/

aria2c https://github.com/adobe-fonts/source-han-sans/releases/download/2.004R/SourceHanSansJ.zip --dir=~/dotfile_downloads
doas mkdir /usr/share/fonts/JP
doas unzip ~/dotfile_downloads/SourceHanSansJ.zip -d /usr/share/fonts/JP/

aria2c https://github.com/adobe-fonts/source-han-sans/releases/download/2.004R/SourceHanSansK.zip --dir=~/dotfile_downloads
doas mkdir /usr/share/fonts/KR
doas unzip ~/dotfile_downloads/SourceHanSansK.zip -d /usr/share/fonts/KR/

aria2c https://github.com/adobe-fonts/source-han-sans/releases/download/2.004R/SourceHanSansSC.zip --dir=~/dotfile_downloads
doas mkdir /usr/share/fonts/SC
doas unzip ~/dotfile_downloads/SourceHanSansSC.zip -d /usr/share/fonts/SC/

mkdir -pv "$HOME/.local/share/fonts/"
cp -r "./Fonts/codicons" "$HOME/.local/share/fonts/"
mkdir $HOME/.local/share/fonts/Win10Fonts/
unzip ./Fonts/Win10Fonts.zip -d $HOME/.local/share/fonts/Win10Fonts/

fc-cache -f -v

## INSTALL suckless software
make --directory=./software/dwm-6.2/ clean install
make --directory=./software/st-0.8.4/ clean install
doas tic -sx ./software/st-0.8.4/st.info
make --directory=./software/dmenu-5.0/ clean install
doas make --directory=./software/slock-1.4/ clean install


echo 'export PATH=$PATH:$HOME/bin' >>~/.bashrc
## INSTALL JAVA
curl --output ~/dotfile_downloads/jdk17.tar.gz https://download.oracle.com/java/17/latest/jdk-17_linux-x64_bin.tar.gz
tar -zxvf ~/dotfile_downloads/jdk17.tar.gz -C ~/dotfile_downloads/
mkdir ~/Java
mv ~/dotfile_downloads/jdk-17.0.1 ~/Java/
echo 'export JAVA_HOME=$HOME/Java/jdk-17.0.1' >>~/.bashrc
echo 'export PATH=$PATH:$JAVA_HOME/bin' >>~/.bashrc

## INSTALL Miniconda
curl --output ~/dotfile_downloads/Miniconda.sh https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
sh ~/dotfile_downloads/Miniconda.sh

## INSTALL JDTLS
curl --output ~/dotfile_downloads/jtls.tar.gz https://download.eclipse.org/jdtls/snapshots/jdt-language-server-latest.tar.gz
mkdir -pv $HOME/.local/share/lsp/jdtls
tar -zxvf ~/dotfile_downloads/jdtls.tar.gz -C ~/.local/share/lsp/jdtls/

## ADD .bin folder
cp -r ./.bin ~/
echo 'export PATH=$PATH:$HOME/.bin' >>~/.bashrc


## EDIT PS1
echo "PS1='> '" >>~/.bashrc

## INSTALL MYENV (for conda)
# conda create -n myenv python=3.8 scipy numpy matplotlib pandas scikit-learn seaborn jupyter
# conda activate myenv
echo "conda activate myenv" >>~/.bashrc
## SETUP TMUX
cp ./.tmux.conf ~/
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

## COPY CONFIG FILES
cp -r ./.config ~/
rm -rf ~/.config/fcitx5 ~/.config/qutebrowser ~/.config/alacritty

## INSTALL NEOVIM
nvim +'hi NormalFloat guibg=#1e222a' +PackerSync

## SETUP WALLPAPERL
mkdir ~/.wallpaper

doas cp ./software/tlp.conf /etc/

## SETUP .xinitrc
cp ./.xinitrc ~/

julia ENV["PYTHON"]="/home/th4tkh13m/miniconda3/envs/myenv/bin/python"
