# THIS IS THE CONFIG FILE OF MY MACHINE

## Philosophy

I love suckless' Philosophy, which means I use simple, minimalist and suckless softwares.

## Measurements

The measurements takes at my laptop, with TLP on. I do use TLP to reduce the performance of my laptop for better battery life. My TLP config file can also be found in this repository.

## WHAT DOES THIS DOTFILE CONTAIN

This dotfile repository contains:

- Configs for bash, profile, Xorg, tmux and fbterm (which are placed in `$HOME`)
- .bin folder (This contains many useful bash scripts I wrote):
  - `color`: Changing the color of the tty
  - `java-compile`: Find and compile all the `java` files in the project folder, then execute the Main class
  - `java-lsp`: Use to run `jdtls` language server
  - `note`: A useful script for quick notes
  - `getpass`: Get the password of a given website to the clipboard, then clear the clipboard after an amount of time
- .config folder (Config of nvim, qutebrowser, fcitx and redshift)
- software folder: This is the place I store the suckless softwares' configs, as well as pacman pkglist and TLP config.

## DEMO

![screen1](/uploads/ce6b26510c94f27cc79930422be8510f/screen1.png)

![screen2](/uploads/3b98c585c009ebca905dd39d4bdb0d2a/screen2.png)

![screen3](/uploads/e080a19715f75f243fdce92e8d29bf9b/screen3.png)

![screen4](/uploads/a49032233359b3aed4451b117bb817b7/screen4.png)

![screen](/uploads/717c12506247edaa1eb938210fea7416/screen.png)

![image](/uploads/d462397a525b9dbd8fcd61842c163faf/image.png)
