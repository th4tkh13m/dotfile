## CHROOT
mount --rbind /sys /mnt/sys && mount --make-rslave /mnt/sys
mount --rbind /dev /mnt/dev && mount --make-rslave /mnt/dev
mount --rbind /proc /mnt/proc && mount --make-rslave /mnt/proc


## INSTALLING USING XBPS METHOD
export REPO=https://alpha.de.repo.voidlinux.org/current
export ARCH=x86_64
XBPS_ARCH=$ARCH xbps-install -S -r /mnt -R "$REPO"  base-files coreutils findutils diffutils grep gzip sed gawk util-linux which tar shadow procps-ng iana-etc xbps tzdata runit-void removed-packages ncurses libgcc dracut file less man-pages mdocml e2fsprogs  dosfstools pciutils usbutils openssh kbd iproute2 iputils iw iwd opendoas wifi-firmware traceroute kmod eudev acpi acpilight alsa-utils aria2 base-devel cmus curl elogind fcitx fcitx-configtool fcitx-unikey feh fuse git gummiboot highlight htop inetutils-ftp iwd libX11-devel libXft-devel libXinerama-devel libXrandr-devel libfcitx-gtk3 libreoffice linux-lts maim mesa mpv neovim openresolv patch picom polkit ranger redshift ripgrep rsync source-sans-pro texlive-bin tlp tmux ueberzug w3m xclip xf86-video-intel xorg-fonts xorg-minimal xsetroot xss-lock xtools zathura zathura-pdf-mupdf


mkdir -pv /mnt/etc/dracut.conf.d
cat << EOF > /mnt/etc/dracut.conf.d/00-dracut.conf
hostonly=yes
hostonly_cmdline=no
use_fstab=yes
compress="cat"
omit_dracutmodules+=" dash i18n rpmversion convertfs btrfs lvm qemu multipath qemu-net lunmask fstab-sys terminfo securityfs img-lib biosdevname caps crypt crypt-gpg dmraid dmsquash-live mdraid nbd nfs network "
nofscks=yes
no_hostonly_commandline=yes
EOF

## INSTALLATION CONFIGURATION
cat << EOF > /mnt/etc/hostname
void
EOF

nvim /mnt/etc/default/libc-locales
chroot /mnt xbps-reconfigure -f glibc-locales

chroot /mnt ln -sfv /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime

## CONFIGURING FSTAB
cp /proc/mounts /etc/fstab
echo "Remove lines in /etc/fstab that refer to proc, sys, devtmpfs and pts."
echo "Edit partion scheme with UUID gtom blkid"
nvim /mnt/etc/fstab

## INSTALL BOOTLOADER
chroot /mnt gummiboot install

## INTEL DRIVER
mkdir -pv /mnt/etc/X11/xorg.conf.d
cat << EOF > /mnt/etc/X11/xorg.conf.d/20-intel.conf
Section "Device"
	Identifier "Intel Graphics"
	Driver "modesetting"
EndSection
EOF

cat << EOF > /mnt/etc/xbps.d/00-repository-main.conf
repository=https://mirrors.dotsrc.org/voidlinux/current
EOF

cat << EOF > /mnt/etc/xbps.d/99-ignore.conf
ignorepkg=linux-firmware-amd
ignorepkg=linux-firmware-nvidia
EOF

cat << EOF > /mnt/etc/iwd/main.conf
[General]
UseDefaultInterface=true
EnableNetworkConfiguration=True
EOF

cat << EOF > /mnt/etc/doas.conf
permit :wheel
EOF
chroot /mnt chown -c root:root /etc/doas.conf
chroot /mnt chmod -c 0400 /etc/doas.conf

chroot /mnt useradd -m -G  wheel,floppy,audio,video,cdrom,optical,kvm,xbuilder -s bash th4tkh13m

chroot /mnt ln -sv /usr/share/fontconfig/conf.avail/70-no-bitmaps.conf /etc/fonts/conf.d/
chroot /mnt xbps-reconfigure -fa

chroot /mnt ln -s /etc/sv/dbus /var/service/
chroot /mnt ln -s /etc/sv/iwd /var/service/
chroot /mnt ln -s /etc/sv/tlp /var/service/
chroot /mnt ln -s /etc/sv/alsa /var/service/
