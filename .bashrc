# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias v="nvim"
alias poweroff="loginctl poweroff"
alias suspend="loginctl suspend"
alias reboot="loginctl reboot"
alias xi="doas xbps-install"
alias xr="doas xbps-remove"
alias xq="doas xbps-query"
alias ranger=". ranger"

# export RANGER_LOAD_DEFAULT_RC=false
# export EDITOR=nvim
export PATH=$PATH:$HOME/.local/bin
export PATH=$PATH:$HOME/Julia/julia-1.6.4/bin
export JAVA_HOME=$HOME/Java/jdk-17.0.1
export PATH=$PATH:$JAVA_HOME/bin

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/th4tkh13m/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/th4tkh13m/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/th4tkh13m/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/th4tkh13m/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

shopt -s autocd
shopt -s checkwinsize

conda activate myenv
PS1='> '
pfetch
